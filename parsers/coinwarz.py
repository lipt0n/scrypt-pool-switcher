from pyquery import PyQuery as pq
from lxml import etree
import urllib
less_is_better=True

def get_coin_short(coin):
	a= coin.split('(')
	b= a[1].split(')')
	return b[0]
def get_coin_list(khs=4300):
	try:
		d = pq(url="http://www.coinwarz.com/miningprofitability/litecoin/?hr={0}.00&p=0.00&pc=0.0000&e=Coinbase".format(khs))
	except:
		exit("can't get data")

	coins = {}
	tr = d("#tblCoins").find('tr')
	for x in range(len(tr)-1):
		name = get_coin_short(pq(pq(tr[x+1]).find('b')[0]).text() )
		btcindays = float(pq(pq(tr[x+1]).find('.btcDays')).text().replace(',','') )
		coins.update({name:btcindays})
	return coins
