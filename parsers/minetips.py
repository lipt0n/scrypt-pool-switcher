from pyquery import PyQuery as pq
from lxml import etree
import urllib
import requests
less_is_better=False

def get_coin_list(khs=4300):

	try:
		user_agent = {'User-agent': 'Mozilla/5.0'}
		r = requests.get("http://www.minetips.com/?khs={0}&mhs=1&spw=400&kwh=0.15".format(khs),headers=user_agent, timeout=40)
		d = pq(r.text)
	except:
		exit("can't get data")

	coins = {}
	tr = d("#currTable").find('tr.roww')
	for x in tr:
		data = pq(x).text().split(' ')
		name = data[0]
		if data[1] == 'scrypt':
			walue = float(data[9] )
			coins.update({name:walue})
	return coins