#!/usr/bin/env python
#encoding: utf-8
import os
import time
from pycgminer import CgminerAPI

import settings
_temp = __import__('parsers.{0}'.format(settings.parser), globals(), locals(), ['get_coin_list', 'less_is_better'], -1)
get_coin_list=_temp.get_coin_list
less_is_better=_temp.less_is_better


#get stats
coins = get_coin_list(khs=settings.khs)
try:
	cgminer = CgminerAPI(host=settings.cgminer_host, port=settings.cgminer_port)
except:
	exit("can't connect to cgminer")

pools = settings.pools
for x in coins:
	if x in pools:
		pools[x]['profit'] = coins[x]
for x in pools:
	if not  pools[x].has_key('profit'):
		if less_is_better:
			pools[x]['profit']=99999;
		else: 
			pools[x]['profit']=0;
#check if pools are listed in cgminer 
p = cgminer.pools()['POOLS']
cgminer_pools = {}
for x in p:
	cgminer_pools.update({x['URL']:x['POOL']})
for x in pools:
	if not pools[x]['url'] in cgminer_pools:
		cgminer.addpool("{0},{1},{2}".format(pools[x]['url'], pools[x]['user'], pools[x]['pass']) )
p = cgminer.pools()['POOLS']
cgminer_pools = {}
for x in p:
	cgminer_pools.update({x['URL']:x['POOL']})
for x in pools:
	pools[x]['cgminer_id'] = cgminer_pools[pools[x]['url']]

sorted_by_profit = sorted(pools.iteritems(), key=lambda x: x[1]['profit'], reverse=(not less_is_better))

best = sorted_by_profit[0][1]['cgminer_id']
print "best is ", sorted_by_profit[0][0], ' profit: ', sorted_by_profit[0][1]['profit']
#print sorted_by_profit
#for x in cgminer.pools()['POOLS']:
#	print x['URL'], ' ',x['POOL']
#print '-----'

cgminer.switchpool(best)
