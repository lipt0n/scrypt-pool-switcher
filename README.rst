Program to check proficy of mining scrypt coins and change active pool on cgminer to the most proficient.



REQUIREMENTS
****
pycgminer: https://github.com/tsileo/pycgminer

requests: https://docs.python-requests.org/en/latest/

pyquery: https://pythonhosted.org/pyquery/





INSTALLATION
****

pip install pycgminer requests pyquery


git clone https://lipt0n@bitbucket.org/lipt0n/scrypt-pool-switcher.git





CONFIGURATION 
****

First edit settings.py , add your pools with names exacly thesame like on website we will check proficienty (default http://www.minetips.com/)

You can also make your own parser like this in parsers/minetips.py





USAGE
****
You have to run your cgminer lihe this::
	cgminer --api-port 4001 --api-listen --api-allow "W:YOUR_SCRYPT_POOL_SWITCHER_IP"

If You want to change port you have to do the same in settings.py


Now you can run program :] good idea is to just set up cron job for this.
Program will switch pools without stoping cgminer work :)





TODO
****
I don't know, You tell me :)
srsly if you have any idea, please email me at *michal.ipinski (at) gmail.com*






if You like it please DONATE:
****

**Bitcoin:** *16879UkumDhMeAqJkqN33zKroWukeXUkJ4*

**Litecoin:** *LcStqhStat4PRFdXauiPX2uiq37rHcMrRW*

**Dogecoin:** *DEUy1WiNxHd39vaFWPLznsuX8Gw1AFNe1a*


I will rly appreciate any donation, I want to build my first mining rig (now i'm just mining on old ati gpu ~ 90kh/s) please help make my dream come true ^__^**
****


I can make more parsers if You need one :)
